import CartList from "../src/components/CartDesigns/CartList";
import { apiAction } from "../src/store/reducer";
import { useDispatch, useSelector } from "react-redux";

export default function Home(props) {
  //console.log(props.newData);

  const dispatch = useDispatch();

  const getData = useSelector((state) => state.allReducer.api);

  console.log("getData -->>", getData);

  // useEffect(() => {
  //   dispatch(apiAction());
  // }, []);

  if (Array.isArray(getData) && getData.length === 0) {
    dispatch(apiAction());
  }

  return (
    <div>
      <CartList data={getData} />
    </div>
  );
}

// export const getServerSideProps = wrapper.getServerSideProps(
//   async ({ store, req }) => {
//     store.dispatch({ type: NEW_API });

//     const getData = store.getState();

//     console.log("getDatas ====>>>>>", await getData.apiD.api);

//     return {
//       props: {
//         newData: "kapil shukla",
//       },
//     };
//   }
// );

// export async function getServerSideProps() {
//   const getJson = await axios("https://jsonplaceholder.typicode.com/photos");

//   const resData = getJson.data;

//   return {
//     props: {
//       newData: resData,
//     },
//   };
// }


